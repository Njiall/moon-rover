var data = (localStorage.getItem('todoList')) ? JSON.parse(localStorage.getItem('todoList')) : {
	todo: [],
	completed: []
};

renderTodoList();
updateStats();

// User clicked on the add button
// If there is any text inside the item field, add that text to the todo list
document.getElementById('mr-task-add').addEventListener('click', function (e) {
	var input = document.getElementById('mr-task-input');
	var value = input.value;
	if (value) {
		addItem(value);
	}
	e.preventDefault();
	input.focus();
});

function addItem(value) {
	addItemToDOM(value);
	document.getElementById('mr-task-input').value = '';

	data.todo.push(value);
	dataObjectUpdated();
}

// If user press enter then validate the input
document.getElementById('mr-task-input').addEventListener('keydown', function (e) {
	var value = this.value;
	if ((e.code === 'Enter' || e.code === 'NumpadEnter') && value) {
		e.preventDefault();
		addItem(value);
	}
});

function progressbarSetProgress(element, progress, message, state_100, state_default) {
	element.setAttribute('text', message);
	element.setAttribute('type', (progress === 100) ? state_100 : state_default);
	element.style.width = progress + "%";
	element.parentNode.style.opacity = 1;
}

function hideProgress(element) {
	element.parentNode.style.opacity = 0;
}

window.setTimeout(function() {
	updateStats();
	window.setInterval(updateStats, 60000);
}, (60 - new Date().getSeconds()) * 1000);
function updateStats() {
	var done = data.completed.length;
	var all = data.todo.length + data.completed.length;
	var progress = (all !== 0) ? done / all * 100: 100;
	var date = new Date();
	if (date.getHours() >= 22) {
		var time = 100;
		var timeLeft = 0;
	} else {
		var time = date.getHours() / 22 * 100;
		var timeLeft = (22 - (date.getHours() % 24)) + ":" + (59 - date.getMinutes());
	}
	var bars = document.getElementsByClassName('mr-progressbar');
	for (var i = 0; i < bars.length; i++) {
		switch (bars[i].getAttribute('title')) {
			case 'Complete' :
			var message = (all === 0) ? "Nothing to do." : (progress === 100) ? "Everything done!" : Math.floor(progress) + "% Completed";
			progressbarSetProgress(bars[i], progress, message, "success", (progress < 25) ? "warning" : "normal");
			break;
			case 'Time' :
			if (all === 0) {
				hideProgress(bars[i]);
			} else {
				var message = (progress === 100) ? "Everything done for today!" : ((timeLeft !== 0) ? timeLeft + " Until the end of the day" : "Day ended without all tasks done!");
				progressbarSetProgress(bars[i], (progress === 100) ? 100 : time, message, (progress === 100) ? "success" : "failure", (date.getHours() >= 19) ? "warning" : "normal");
			}
		}
	}
}

function dataObjectUpdated() {
	localStorage.setItem('todoList', JSON.stringify(data));
	updateStats();
}

function removeItem() {
	var parent = this.parentNode;
	var children = parent.childNodes;
	var label = this.previousSibling;
	var value = label.innerText;

	if (!parent.classList.contains("hide")) {

		if (parent.parentNode.id === 'mr-list-todo') {
			data.todo.splice(data.todo.indexOf(value), 1);
		} else {
			data.completed.splice(data.completed.indexOf(value), 1);
		}
		dataObjectUpdated();

		parent.classList.add("hide");
		window.setTimeout(function(element, parent) {
			element.removeChild(parent);
		}, 200, parent.parentNode, parent);
	}
}

function completeItem(e) {
	var item = this.parentNode;
	var parent = item.parentNode;
	var id = parent.id;
	var value = this.innerText;

	e.preventDefault();
	if (id === 'mr-list-todo') {
		item.firstChild.checked = true;
		data.todo.splice(data.todo.indexOf(value), 1);
		data.completed.push(value);
	} else {
		item.firstChild.checked = false;
		data.completed.splice(data.completed.indexOf(value), 1);
		data.todo.push(value);
	}
	item.classList.add("hide");
	window.setTimeout(function(element) {
		element.classList.remove("hide");
	}, 100, item);
	dataObjectUpdated();

	// Check if the item should be added to the completed list or to re-added to the todo list
	var target = (id === 'mr-list-todo') ? document.getElementById('mr-list-done') : document.getElementById('mr-list-todo');

	target.insertBefore(item, target.childNodes[0]);
}

// Adds a new item to the todo list
function addItemToDOM(text, completed) {
	var list = (completed) ? document.getElementById('mr-list-done') : document.getElementById('mr-list-todo');

	var uid = guid();
	var elem = document.createElement("div");
	elem.className = "mr-input mr-input-prevent";

	var label = document.createElement("label");
	label.htmlFor = "mr-task-" + text + "-" + uid;
	label.addEventListener('click', completeItem);

	var input = document.createElement("input");
	input.setAttribute("type", "checkbox");
	if (completed) {
		input.setAttribute("checked", "");
	}
	input.id = "mr-task-" + text + "-" + uid;

	var remove = document.createElement("input");
	remove.setAttribute("type", "button");
	remove.className = "close";
	remove.onclick = "closeTask(elem)";
	// Add click event for removing the item
	remove.addEventListener('click', removeItem);

	var t = document.createTextNode(text);
	label.appendChild(t);
	elem.appendChild(input);
	elem.appendChild(label);
	elem.appendChild(remove);
	list.insertBefore(elem, list.childNodes[0]);
}

function renderTodoList() {
	if (!data.todo.length && !data.completed.length) return;

	for (var i = 0; i < data.todo.length; i++) {
		var value = data.todo[i];
		addItemToDOM(value);
	}

	for (var j = 0; j < data.completed.length; j++) {
		var value = data.completed[j];
		addItemToDOM(value, true);
	}
}

function guid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
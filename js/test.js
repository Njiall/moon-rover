
function createTask(inputValue, store) {
	var uid = guid();
	var list = document.getElementById("mr-list-todo");
	var elem = document.createElement("div");
	elem.className = "mr-input";
	var label = document.createElement("label");
	label.htmlFor = "mr-task-" + inputValue + "-" + uid;
	var input = document.createElement("input");
	input.setAttribute("type", "checkbox");
	input.id = "mr-task-" + inputValue + "-" + uid;
	var close = document.createElement("input");
	close.setAttribute("type", "button");
	close.className = "close";
	close.addEventListener('click', removeItem);
	var t = document.createTextNode(inputValue.charAt(0).toUpperCase() + inputValue.slice(1));
	label.appendChild(t);
	elem.appendChild(input);
	elem.appendChild(label);
	elem.appendChild(close);
	list.appendChild(elem);
	if (store)
		localStorage.setItem('task-' + uid, inputValue);
}

function createTaskFromInput() {
	var inputValue = document.getElementById("task-input").value;
	document.getElementById("task-input").value = '';
	if (inputValue === '') {
		return;
	}
	createTask(inputValue, true);
}
function toggleOffCanvasRight() {
	var body = document.getElementsByClassName('mr-oc-push')[0];
	var panel = document.getElementsByClassName('mr-oc-panel')[0];
	var attr = (panel.getAttribute('aria-expanded') === 'true') ? 'false' : 'true';
	panel.setAttribute('aria-expanded', attr);
	body.setAttribute('aria-expanded', attr);
}
var digits = [
	[
		1, 1, 1,
		1, 0, 1,
		1, 0, 1,
		1, 0, 1,
		1, 1, 1
	],
	[
		1, 1, 0,
		0, 1, 0,
		0, 1, 0,
		0, 1, 0,
		1, 1, 1
	],
	[
		1, 1, 1,
		0, 0, 1,
		1, 1, 1,
		1, 0, 0,
		1, 1, 1
	],
	[
		1, 1, 1,
		0, 0, 1,
		1, 1, 1,
		0, 0, 1,
		1, 1, 1
	],
	[
		1, 0, 1,
		1, 0, 1,
		1, 1, 1,
		0, 0, 1,
		0, 0, 1
	],
	[
		1, 1, 1,
		1, 0, 0,
		1, 1, 1,
		0, 0, 1,
		1, 1, 1
	],
	[
		1, 1, 1,
		1, 0, 0,
		1, 1, 1,
		1, 0, 1,
		1, 1, 1
	],
	[
		1, 1, 1,
		0, 0, 1,
		0, 0, 1,
		0, 0, 1,
		0, 0, 1
	],
	[
		1, 1, 1,
		1, 0, 1,
		1, 1, 1,
		1, 0, 1,
		1, 1, 1
	],
	[
		1, 1, 1,
		1, 0, 1,
		1, 1, 1,
		0, 0, 1,
		1, 1, 1
	]
];

function lightDigit(n, number) {
	var clock = document.getElementById('mr-clock');
	var num = clock.children[n];
	for (var i = 0; i < 15; i++) {
		if ((num.children[i].getAttribute("state") !== null) !== (digits[number][i] > 0)) {
			(digits[number][i] > 0) ? num.children[i].setAttribute("state", "") : num.children[i].removeAttribute("state");
		}
	}
}

function lightIndicator(date) {
	var index = date.getSeconds();
	var clock = document.getElementById('mr-clock');
	var nums = clock.children;
	var k = 0;
	for (var i = 0; i < 4; i++) {
		for (var j = 0; j < 15; j++, k++) {
			if (nums[i].children[j].getAttribute('counter') !== null) {
				nums[i].children[j].removeAttribute('counter');
			}
			if (k == index) {
				nums[i].children[j].setAttribute('counter', "");
			}
		}
	}
}

function animateClock() {
	var date = new Date();

	lightIndicator(date);

	var hours = (date.getHours() < 10) ? '0' + date.getHours().toString() : date.getHours().toString();
	var minutes = (date.getMinutes() < 10) ? '0' + date.getMinutes().toString() : date.getMinutes().toString(); 
	lightDigit(0, parseInt(hours[0]));
	lightDigit(1, parseInt(hours[1]));
	lightDigit(2, parseInt(minutes[0]));
	lightDigit(3, parseInt(minutes[1]));

	window.requestAnimationFrame(animateClock);
}

window.requestAnimationFrame(animateClock);